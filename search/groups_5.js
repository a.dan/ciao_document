var searchData=
[
  ['6_29_20break_2dup_20parameters',['6) Break-up Parameters',['../group__breakup.html',1,'']]],
  ['6_29_20flame_20kernel_20model',['6) Flame Kernel Model',['../group__flame__kernel.html',1,'']]],
  ['6_29_20numerical_20settings',['6) Numerical Settings',['../group__numerical__settings.html',1,'']]],
  ['6_29_20lagrangian_20wall_20film_20module',['6) Lagrangian Wall Film Module',['../group__wall__film.html',1,'']]]
];
