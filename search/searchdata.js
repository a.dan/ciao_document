var indexSectionsWithContent =
{
  0: "12345678abcdefghijklmnopqrstuvwxyz",
  1: "abcdefghilmnopqrstuvwx",
  2: "abcdefghilmnopqrstuv",
  3: "abcdefghilmnopqrstuvwx",
  4: "abcdefghijlmnopqrstuvwxz",
  5: "abcdefghijklmnopqrstuvwxyz",
  6: "12345678abcdefghilmnoprstuvwxz",
  7: "adfhilmrsw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "groups",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Modules",
  7: "Pages"
};

