var searchData=
[
  ['data',['data',['../namespacedata.html',1,'']]],
  ['data_5fxx_5fdefs',['data_xx_defs',['../namespacedata__xx__defs.html',1,'']]],
  ['data_5fxx_5ffunc',['data_xx_func',['../namespacedata__xx__func.html',1,'']]],
  ['data_5fxx_5fget',['data_xx_get',['../namespacedata__xx__get.html',1,'']]],
  ['data_5fxx_5fregister',['data_xx_register',['../namespacedata__xx__register.html',1,'']]],
  ['deallocate_5fptr_5fm',['deallocate_ptr_m',['../namespacedeallocate__ptr__m.html',1,'']]],
  ['density_5fpulse',['density_pulse',['../namespacedensity__pulse.html',1,'']]],
  ['dump_5fvtk',['dump_vtk',['../namespacedump__vtk.html',1,'']]],
  ['dvode_5ff90_5fm',['dvode_f90_m',['../namespacedvode__f90__m.html',1,'']]]
];
