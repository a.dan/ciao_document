var searchData=
[
  ['ncv_5ft_5favg',['ncv_T_avg',['../group__ncv__Temp__avg.html',1,'']]],
  ['no_20of_20cells_20above_20piston',['No of cells above piston',['../group__no__cells__piston.html',1,'']]],
  ['npx_20_2f_20npy_20_2f_20npz',['npx / npy / npz',['../group__npxnpynpz.html',1,'']]],
  ['nscbc_20outlet_20damper_20length',['NSCBC outlet damper length',['../group__nscbc__outlet__damp__len.html',1,'']]],
  ['nscbc_20outlet_20damper_20visc',['NSCBC outlet damper VISC',['../group__nscbc__outlet__damp__visc.html',1,'']]],
  ['nscbc_20use_20alt',['NSCBC use alt',['../group__nscbc__use__alt.html',1,'']]],
  ['number_20of_20droplets',['Number of droplets',['../group__num__droplets.html',1,'']]],
  ['number_20of_20nozzle_20holes',['Number of nozzle holes',['../group__num__noz__holes.html',1,'']]],
  ['nx_20_2f_20ny_20_2f_20nz',['nx / ny / nz',['../group__nxnynz.html',1,'']]],
  ['nx_20_2f_20ny_20_2f_20nz',['nx / ny / nz',['../group__nxnynz__init__flow.html',1,'']]]
];
