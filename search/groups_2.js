var searchData=
[
  ['3_29_20spatially_2d_20and_20time_2daveraged_20statistics',['3) Spatially- and Time-averaged Statistics',['../group__average__stats.html',1,'']]],
  ['3_29_20complex_20geometries',['3) Complex Geometries',['../group__complex__geometries.html',1,'']]],
  ['3_29_20convert_5fprofiles',['3) Convert_profiles',['../group__convert__profiles.html',1,'']]],
  ['3da_20config',['3da config',['../group__da__config.html',1,'']]],
  ['3_29_20evaporation_20parameters',['3) Evaporation Parameters',['../group__evaporation.html',1,'']]],
  ['3_29_20field_20operations',['3) Field Operations',['../group__field__operations.html',1,'']]],
  ['3_29_20flow_20field_20initialization',['3) Flow Field Initialization',['../group__flow__field__init.html',1,'']]],
  ['3_29_20hybrid_20lagrangian_20eulerian_20module',['3) Hybrid Lagrangian Eulerian Module',['../group__hyLEM.html',1,'']]],
  ['3_29_20mesh_20motion',['3) Mesh Motion',['../group__mesh__motion.html',1,'']]],
  ['3_29_20multiphase_20module',['3) Multiphase Module',['../group__multiphase.html',1,'']]],
  ['3_29_20pressure_20boundary_20profiles',['3) Pressure Boundary Profiles',['../group__pressure__boundary.html',1,'']]],
  ['3_29_20shock_20capturing',['3) Shock Capturing',['../group__shock__capturing.html',1,'']]],
  ['3_29_20simulation_20end_20criteria',['3) Simulation End Criteria',['../group__simulation__end__criteria.html',1,'']]],
  ['3_29_20spark_20channel_20model',['3) Spark Channel Model',['../group__spark__channel__model.html',1,'']]],
  ['3_29_20spark_20kernel_20transfer_20to_20combustion_20model',['3) Spark Kernel Transfer to Combustion Model',['../group__spark__transfer.html',1,'']]],
  ['3_29_20special_20output_20for_20pre_2ddefined_20cases',['3) Special Output for Pre-Defined Cases',['../group__special__output.html',1,'']]]
];
